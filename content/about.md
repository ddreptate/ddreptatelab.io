---
title: "A propos"
description: "Le garçon dans ''Les habits neufs de l’empereur'' chez mes clients, Tyrion Lannister chez Benext" 
date: 2019-03-29T22:50:06+01:00
draft: false
weight: -110
menu:
  main: {}
  
---

<img src="/images/emperors_new_clothes_550.png" alt="le roi" style="float: left; width: 40%; margin-right: 20px"/>


À l’instar du petit garçon dans [Les habits neufs de l’empereur](https://fr.wikipedia.org/wiki/Les_Habits_neufs_de_l%27empereur), j'aide les organisations à se poser les bonnes questions et à trouver des réponses dans leur chemin de transformation qui passe par une prise de conscience de l’écart entre ce que les organisations pensent être, ce qu’elles sont vraiment et ce qu’elles veulent devenir.  

Ma mission : bâtisseur de possible, bâtisseur de demain.  

Ma raison d’être : donner du sens et aider les gens à trouver la joie et la fierté dans ce qu’ils accomplissent.
