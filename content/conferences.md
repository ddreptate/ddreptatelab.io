---
title: "Conferences"
description: "Quelques interventions en tant qu'orateur et/ou organisateur. "
date: 2019-03-29T22:50:06+01:00
draft: false
featured_image: "/images/leonardo.jpg"
weight: -120
menu:
  main: {}
---

**2019**

- [School of Product Ownership](http://laconf.schoolofpo.com) - déuxieme édition de la conférence **School of Product Ownership** avec un mojo _#ProductRevolution #NoVanityMetrics #FaceYourProduct_ 


---
**2018**

- [School of PO](http://2018.schoolofpo.com) - organisation de la première édition de la conférence **School of Product Ownership** avec [Pablo](http://pablopernot.fr). Inauguré par notre Franky, le thème de cette première édition était _#BuildMeBaby_  

- [FlowCon France](http://www.flowcon.fr) - Co-organisateur de la première édition de FlowCon qui prend la suite de Lean Kanban France. Nouvelle identité, nouveau souffle, mais toujours la recherche d'excellence.  

---
 
**2017**

- [Meetups School of PO](https://www.meetup.com/fr-FR/School_of_PO/) - lancement de la série de meetups **School of Product Ownership**

- Agile En Seine / Agile Grenoble / Agile Paris by Night : atelier [Protocoles de prise de décision ][1]

- Lean Kanban France - membre de l'équipe d'organisation

---
**2016**

- Agile Tour Lille - [Gothamocratie – quand Gotham City adopte l'holacratie][2] avec [Pablo][3] et [Géraldine][4]

- Agile France - [Gothamocratie – quand Gotham City adopte l'holacratie][6] avec [Pablo][3] et [Géraldine][4]. Quelques retours [ici][7] et [ici][8].

- Lean Kanban France - Membre de l'équipe d'organisation

- [ALE 2016 Paris Unconference](/2016/09/ale16/) - Membre de l'équipe d'organisation

---
**2015**

- Scrum Day - Atelier [« Le chemin le plus court vers le succès : Impact Mapping »][9] avec [Géraldine][4]. On en parle [ici][10]

- Lean Kanban France - Membre de l'équipe d'organisation

---
**2014**

- Scrum Day - Atelier [« Kapla Challenge »][11]

---
**2013**

- Scrum Day - Atelier « Perdus dans le désert ». Quelques retours [ici][12] et [ici][13].

---
**2012**

- Scrum Day - [Le Product Owner comme agent de la transformation agile][14] avec [Oana][15].

---
**2011**

- Agile Tour Paris - [Une étape importante dans la transformation Agile : Créer le cercle de compétences PO][16] avec [Oana][15].

[1]: /2018/01/05/protocoles-de-prise-de-decision/
[2]: http://2016.agiletour-lille.org/conferences/#Gothamocratie,%20quand%20Gotham%20City%20adopte%20l'holacratie
[3]: http://pablopernot.fr/
[4]: http://www.theobserverself.com/
[5]: http://2016.leankanban.fr/programme-en-cours-de-creation/
[6]: http://lanyrd.com/2016/agilefrance/sfbfqw/
[7]: http://www.theobserverself.com/rencontres/catwoman-coaching/
[8]: http://blog.soat.fr/2016/07/agile-france-2016-gothamocratie-batman-nous-fait-decouvrir-lholacratie/
[9]: http://fr.slideshare.net/ddreptate/workshops-impact-mapping
[10]: http://blog.soat.fr/2015/04/scrumday-2015-le-chemin-le-plus-court-vers-le-succes-impact-mapping/
[11]: http://fr.slideshare.net/ddreptate/kapla-challenge-scrum-day-2014
[12]: http://blog.viseo-bt.com/perdu-dans-le-desert-scrumdayfr-2013/
[13]: http://blog.soat.fr/2013/06/scrum-day-2013-journal-de-bord/
[14]: https://prezi.com/v6e_vzvbomg7/le-product-owner-comme-agent-de-la-transformation-agile/
[15]: https://twitter.com/ojuncu
[16]: http://at2011.agiletour.org/conferencecontent.html?sid=7461

  
