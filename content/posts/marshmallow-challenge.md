---
title: "Marshmallow Challenge"
date: 2017-01-16
draft: false
aliases: [/2017/01/16/marshmallow-challenge/]


---
![le train de montparnasse][6]
## L'agilité et les jeux

Dans mes interventions de coaching il m'arrive souvent, surtout au début de l'accompagnement, de me frotter à la question « mais enfin, c'est quoi agile? ». La réponse à cette question peut prendre des formes très différentes: plus formelle, sous forme d'un séminaire ou une journée de formation comme ça peut être tout aussi bien des conversations et échanges plus informels. Dans tous les cas, le but est de partager, clarifier et s'aligner sur une vision commune de ce que nous mettons derrière le mot « agile » pour poser un cadre et nous donner des références communes sur lesquelles je vais m'appuyer tout au long de l'accompagnement.

Peu importe la forme, ces échanges ne dérogent jamais du précepte « dis-moi et j'oublierai,  montre-moi et je me souviendrai,  implique-moi et je comprendrai » (merci Confucius). Et pour impliquer les gens, rien de mieux que la mise en situation par des ateliers de jeux agiles. Ces jeux sont des vraies métaphores de notre quotidien et représentent le coeur de la mécanique d'apprentissage.

Un de mes ateliers favoris est le [Marshmallow Challenge][1]. Je ne vais pas m'attarder à vous le décrire, si vous ne le connaissez pas encore vous pouvez le découvrir [ici][1], [ici][2] ou [ici][3].

J'utilise cet atelier depuis plusieurs années, toujours avec autant de succès et de plaisir. Il est, à mon avis, un des jeux les plus adaptés pour déclencher des conversations riches autour de l'agilité. Et puis j'ai découvert [un truc conseillé][4] par [Pablo][5], que j'ai intégré à mes animations, et c'est encore mieux.

## Le défi du « trop de … »

Récemment j'ai du animer un Marshmallow Challenge au sein d'une organisation qui se bat contre le trop de cadrage, trop de planification, trop d'estimation, trop de « big up-front design ». Une situation qui empiète sur le démarrage effectif des projets et qui encours de vrais risques quand à leur succès. Car pour réussir il faut s'adapter. Pour s'adapter il faut du feedback. Et le vrai feedback est celui qui se fait sur des choses finis. Mais pour finir, il faut d'abord commencer.

J'avais besoin de mettre en exergue cette situation, de provoquer une prise de conscience et déclencher un dialogue pour avancer. L'idée est venu naturellement. Et si je demandais aux équipes de me donner une estimation de la hauteur de la structure qu'ils pensent construire avant de s'y lancer ? Mettre en évidence leur « big up front design » pour en tirer des enseignements ? J'intègre donc une nouvelle règle au jeu. Une fois le compte à rebours lancé avec les 18 minutes, chaque équipe doit fournir une estimation de la hauteur de leur futur structure avant de toucher aux spaghettis et de commencer la réalisation. Ils peuvent, bien sûr, prendre le temps qu'ils veulent, mais tant qu'on a pas d'estimation, pas le droit de construire.

Alors, voyons comment cela s'est passé. Je lance le compte à rebours et je rappelle encore une fois aux équipes qu'elle doivent fournir une estimation avant de s'y attaquer à la construction, et pas plus, juste un chiffre. Puis j'attends leurs estimations que je note sur un paper board en traçant aussi le temps qu'ils ont mis pour la fournir. Il est habituel que les équipes passent pas mal du temps au début pour faire un plan, imaginer la structure idéale, organiser son travail. En demandant des estimations ce comportement s'est encore plus amplifié. Une des équipe est allé jusqu'à passer 10 minutes (sur 18) à planifier et estimer avant de toucher le premier spaghetti. D'une manière générale toutes les équipes ont passé quasiment la moitié du temps à produire une estimation et l'autre moitié à construire. Inutile de vous dire que sur les 4 équipes, il y a une seule qui au final a eu une structure qui tenait debout. C'est (le hasard?) l'équipe qui a mis le moins de temps à estimer.

## Le debriefing sur l'estimation

Le moment du debrief venu, nous nous sommes posé quelques questions.

**_A quoi servait cette estimation ?_** La seule réponse que j'ai eu fut « tu nous l'as demandé » ! Une bonne occasion pour rappeler le principe de #NoEstimates, « focus on what matters » (se concentrer sur ce qui compte). Pour faire la part entre ce qui compte et ce qui compte pas, on va s'interroger sur comment on va utiliser l'estimation, quelle décision prendrons-nous sur la base de cette estimation ?

**_Le fait d'estimer a-t-il contribué ou amélioré le résultat final ?_** Non, au contraire, on a perdu une bonne partie du temps donné à faire quelque chose d'inutile. Et le fait de planifier et d'estimer n'a pas contribué au résultat final car, une fois la construction entamé, le plan initial c'est avéré faux et l'équipe à du s'adapter à la réalité. Les spaghettis ne sont pas un système prédictible ! 

**_L'estimation a été proche du résultat final ?_** Non plus. Trois équipes sur quatre n'ont pas terminé avec une structure debout. Une d'entre elle avait même estimé pouvoir réaliser une structure de 120 cm ?!? Les structures moyennes tournent entre 40 et 60 cm, autant vous dire que passer 10 minutes pour donner un chiffre aussi hasardeux est un vrai gaspillage.

Les enseignements que le groupe a tiré de cette experience? Aussi surprenant que cela puisse paraître, des spaghettis avec un marshmallow au sommet représente beaucoup d'incertitude. Et en conditions d'incertitude mieux vaut prototyper et affiner, avancer par tâtonnement et optimisation que de planifier et estimer sans confronter nos idées et nos hypotheses à la réalité du terrain.

 

[1]: https://www.tomwujec.com/marshmallowchallenge
[2]: http://www.areyouagile.com/2011/04/les-enfants-et-le-marshmallow-challenge/
[3]: http://sogilis.com/blog/marshmallow-challenge/
[4]: http://www.areyouagile.com/2014/11/truc-pour-le-marshmallow-challenge/
[5]: http://pablopernot.fr
[6]: /images/Train_wreck_at_Montparnasse_1895-750x900.jpg
