---
title: "Le coût de la fausse bienveillance"
date: 2019-11-24
draft: false
#featured_image : "/images/red-pill-blue-pill.jpg"


---
![red-pill-blue-pill](/images/red-pill-blue-pill.jpg)

> This is your last chance. After this, there is no turning back. You take the blue pill — the story ends, you wake up in your bed and believe whatever you want to believe. You take the red pill — you stay in Wonderland, and I show you how deep the rabbit hole goes. Remember: all I'm offering is the truth. Nothing more.
>
>-- Morpheus (Matrix)

<br>
Dans mon quotidien de coach, je croise dans les organisations que j'accompagne des personnes qui prônent la bienveillance comme une de leur valeur. Une valeur souvent invoquée à toutes les sauces, sous une forme qui, à mon sens, n'a que rarement de lien avec la bienveillance.

Derrière cette "fausse bienveillance" affichée en surface, je découvre dans les entrailles de ces organisations une toute autre réalité : de l’inaction chronique, des non-dits qui génèrent des conflits latents, non assumés, une autonomie qui se transforme en perte d’alignement et en comportements nocifs. Le tout parsemé par une bonne dose d’hypocrisie institutionnelle. Des situations souvent générés et nourries par cette posture, une posture qui fait le choix de l'évitement et du manque de courage. Une posture qui flatte les ego, pousse aux oubliettes les sujets importants et étouffe les vraies conversations, confrontantes. Une posture qui empêche, individuellement et collectivement, de progresser et d'aller vers la recherche d'excellence.

Cette emprise de la fausse bienveillance, je la refuse.
