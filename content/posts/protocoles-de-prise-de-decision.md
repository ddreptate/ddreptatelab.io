---
title: "Protocoles de prise de décision"
date: 2018-01-05
draft: false
#featured_image : "/images/leonardo.jpg"
aliases: [/2018/01/05/protocoles-de-prise-de-decision/]


---

L'année 2017 maintenant terminée, je souhaitais partager avec vous l'un des ateliers qui a rythmé mon année avec sa présence dans trois conférences: [Agile Paris by Night][1], [Agile en Seine][2] et [Agile Grenoble][3].

## Pourquoi un atelier sur la prise de décisions?

Dès lors que nous sommes confrontés à la prise d'une décision entre plusieurs personnes, on utilise, plus ou moins consciemment, un protocole de décision. Décider est au cœur du fonctionnement d'une équipe ou d'un groupe. La façon de décider défini la manière dont le pouvoir et le contrôle sont exercés. On peut donc voir les processus de décision comme étant le code génétique d'une organisation, la partie invisible de l'iceberg qui façonne son fonctionnement. D'où l'intérêt de comprendre et de clarifier comment les décisions sont prises au sein d'un groupe ou d'une organisation. Il n'y a pas de bons ou des mauvais protocoles, la vraie question est de savoir comment et surtout quand les utiliser. De décider de comment décider.

Car en fonction du protocole utilisé ou de la situation dans laquelle on l'utilise, la décision finale ne sera pas la même. Prendre conscience de comment la décision est impactée par la manière de décider est essentiel pour réussir à tirer profit de l'intelligence collective.

Aussi, certains protocoles de décisions sont des leviers pour donner plus d'autonomie et responsabiliser, facteurs nécessaires pour obtenir une organisation apprenante, fluide, adaptative avec à la clé plus d'innovation et une meilleure performance.

## De quoi s'agit-il ?

Pour [paraphraser George Box][4], je dirais que _"tous les modèles de prise de décision sont faux, mais certains sont utiles"_.  
Le but de cet atelier est de faire découvrir et pratiquer quelques-uns des protocoles les plus courants par une mise en situation suivie d'un échange en groupe. Un prétexte pour se poser des questions. Quels sont les points forts de chaque protocole ? Et les points faibles ? Quel protocole est plus adapté pour une situation ou un contexte donné ? Comment le choix du protocole influence-t-il la décision prise ? Et quel est l'impact sur l'autonomie et la responsabilisation ?

Au fil de ces trois conférences l'atelier n'a pas cessé d'évoluer, vous avez ici la version la plus aboutie, celle présenté à Agile Grenoble.  


<iframe 
    src="//www.slideshare.net/slideshow/embed_code/key/r7EW6Tmo4Hq0aA" 
    width="595" 
    height="420" 
    frameborder="0" 
    marginwidth="0" 
    marginheight="0" 
    scrolling="no" 
    style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" 
    allowfullscreen> 
</iframe> 

Et pour finir, je tiens à remercier [Olivier][5] et [Samuel][6], sans qui cet atelier n'aurait pas existé. Samuel, pour m'avoir inspiré avec sa première version de l'atelier, version que j'avais reprise et enrichie et Olivier pour m'avoir aidé à la faire évoluer et à l'animer à plusieurs reprises.

N'hésitez pas à me contacter si vous voulez animer cet atelier au sein de votre organisation.

Bonne année et bonnes décisions ! 😉

[1]: http://apbn2017.agileparis.org/
[2]: http://www.agileenseine.com/
[3]: http://agile-grenoble.org/#!/home
[4]: https://en.wikipedia.org/wiki/All_models_are_wrong
[5]: http://ajiro.fr/authors/albiez_olivier/
[6]: http://ajiro.fr/authors/retiere_samuel/
