---
title: "Once Upon a Time... #ALE16"
date: 2016-09-20
draft: false
aliases: [/2016/09/20/ale16/]


---
![le train de montparnasse](/images/ale-amphi.png)

Once upon a time, there was an amazing unconference named ALE, [Agile Lean Europe][1]. ALEsome people get together every year since 2011, each year in a different city in Europe. They get together to share, because they care. Kids and spouses are welcome. It's run by the community for the community. No difference between speakers and participants. No special privileges.

From the very beginning I liked these principles that make ALE so special. And every single year, I had a big constraint that stopped me to attend ALE. Every year I ended up in front of the screen catching up with the uncoference on Twitter. That had to stop! 😀 That's why this year I decided to be part of the organizing team and help as much as I can to bring ALE16 to Paris. 'Cause if you're an organizer, you can't miss the event, right?

And eventually we did it, [ALE16 in Paris][3] became a reality and turned out into an amazing event ! Here is my feedback, more from the perspective of a busy organizer than a very present participant.

## What I liked

* Pablo‘s infinite energy, Olaf‘s soulful-ness, Oana‘s and Franck‘s dedication to make ALE an amazing experience for kids.
* Sharing a full afternoon with my son during the « Coding Gouter » workshop. Kids pairing with parents and learning together to code. Priceless experience!
{{< tweet 770643659943444480 >}}

* The power of self-organisation. I’m always amazed by the richness and the quality of open-space marketplaces.
{{< tweet 770568844628942849 >}}

* Starting the first day by reconnecting body and mind. I felt the crowd so energized and focused afterwards.
{{< tweet 770179760144183297 >}}

* The opening and the closing circle, an inclusive and caring moment
{{< tweet 770167603386019840 >}}

* Evening and night activities, the conference after the conference. Drinks on Quais de Seine, diner with a stranger or a new friend, dancing… a cocktail of casual events that make ALE such an unforgettable moment.
{{< tweet 769972473379098624 >}}

* The self-made badges
{{< tweet 770192008006799360 >}}

* People, people and people. From all over the world ! Even from Alaska
{{< tweet 770281246362664960 >}}

* Making new friends around a good bottle of wine.
{{< tweet 770579443979083776 >}}

## (Some) People I met

* Kurt, you're the most passionate [salsa dancer][4] I ever met !
* [David][5], thank you for looking into my eyes for 5 looong minutes ! 😀
* The Lithuanian team, a oasis of joy and freshness. [Donata, Ornela][4], Ieva, Tadas, Mindaugas – you all rock !
* [Daniel][6], our favorite wine expert !  
<img src="/images/daniel.jpg" alt="Daniel" style="width: 200px"/>

* The conversations with [Albina][8] and [Nathanael][9] during diner with a stranger and the moment when I'm bashing on Americans and find out that my conversation partner is a Yankee ! 😀

## My take aways

* When organizing an event, don't forget the vegans ! 😀
* I already experienced this before and I still haven't find out a good solution: as an organizer, I missed a lot of great sessions, discussions, fun… Can't wait for ALE17 to catch up !
* It's not the program that makes a great event, it's the people who show up.
* « What kind of X ? » and « Is there anything else about X ? » #clean #language
* Unconference, open-space… some people might find it unconfortable to at the beginning, having hard time to find their place and benefit from it. But once they get in, they love it.
* Failure is an option. Some can stand it and some not. It takes courage to go beyond certainty and take the road less traveled.

## People talking about #ALE16:

* [Our 3 Key Takeaways From the ALE Conference][44] feedback from [Kasia][45] and [Rafal][46]
* And some photos [here][47]. 

See you all [next year][10] !

[1]: http://alenetwork.eu/
[3]: http://ale2016.eu
[4]: https://twitter.com/AgileLeanEu/status/770381029123170304
[5]: https://twitter.com/dgheath21
[6]: https://twitter.com/danielfrey63
[7]: http://www.andwhatif.fr/wp-content/uploads/2016/09/Copy-of-IMG_20160830_210634-225x300.jpg?w=100
[8]: https://www.linkedin.com/in/albina-popova-a5bb49a
[9]: https://twitter.com/coachcre8tives
[10]: http://ale-network.ideascale.com/a/ideas/top/campaigns/52258
[44]: http://blog.u2i.com/ale2016/
[45]: https://twitter.com/katiryniak
[46]: https://twitter.com/rafalcymerys/
[47]: https://drive.google.com/drive/u/0/folders/0B77wygz_NqYpZ1BOMnpOWnBSSmM