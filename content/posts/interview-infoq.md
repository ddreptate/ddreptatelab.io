---
title: "Interview dans la série « La Voix du Coach » sur InfoQ"
date: 2016-06-05
draft: false
aliases: [/2016/06/05/interview-serie-voix-coach-infoq/]


---
[InfoQ FR][1] a lancé cet année [« La Voix du Coach »][2], une série des interviews avec ces accompagnateurs de l'agilité, souvent appelés « coachs », pour démêler les discours, donner une voix et un visage à ce « métier », partager leurs  pratiques et façons d'intevenir dans les organisations.

J'ai eu le privilege d'être rapproché par [Stéphane Wojewoda][3], rédacteur de cette série, pour être une de ces « voix » qui partage leur vision sur le coaching, leur parcours, leurs réussites et échecs, ainsi que des conseils et éléments de réflexion actuels.

Voici l'interview dans son integralité (publié initialement [ici][4])

---

_A l'heure du digital et de l'Agile à l'échelle de l'entreprise, les offres pour accompagner les équipes et organisations dans ces mutations se multiplient, et la transition reste toujours délicate et longue. Pour démêler les discours, InfoQ FR propose un panorama hétéroclite de ces accompagnants de l'agilité, souvent appelés "coachs", pour donner une voix à ce "métier", et aussi un visage, des pratiques, des modes d'agir dans les organisations._

_Cette série propose la vision de coachs français sur leur définition du coaching, leur parcours, leurs réussites et échecs, ainsi que des conseils et éléments de réflexion actuels._
 
_Dragos Dreptate se retrouve souvent dans le rôle du petit garçon dans [Les Habits neufs de l'empereur][5]. Il aide les organisations à prendre conscience de l’écart entre ce que les gens pensent être leur organisation, et ce qu’elle est vraiment._


_**InfoQ FR: Pourriez-vous vous présenter et expliquer à nos lecteurs votre définition de votre métier ?**_

>_**Dragos Dreptate**: Tout d’abord, je vous remercie pour l’invitation ! Je me présente la plupart du temps comme “coach Agile”. C’est une sorte de mot valise, un fourre-tout où s’entassent diverses choses, à tel point qu’aujourd’hui il y a autant de définitions que de coachs Agile. A la fois mentor, facilitateur et formateur, je vois mon rôle comme celui d’un agent de changement au niveau organisationnel, tout en faisant la distinction avec le métier de coach en développement personnel. Le terme de coach me plaît bien car il rappelle l’importance de l’aspect humain dans mon travail._
>
> _En même temps, l'ambiguïté de ce terme me gène aussi. Comme Einstein le disait si bien, si on n’est pas capable d’expliquer un concept à un enfant, c’est qu’on ne le comprend pas assez bien. J’éprouvais cette difficulté avec mon fils quand on parlait de mon travail, jusqu’au jour où un formidable projet de sa classe m’a fait repenser à Saint-Exupery : “pour ce qui est de l'avenir, il ne s'agit pas de le prévoir, mais de le rendre possible”. C’est comme ça que je perçois ma mission : être un bâtisseur, bâtisseur de possible, bâtisseur de demain. Ce qui m’anime est de contribuer à transformer le monde du travail pour lui (re)donner du sens et aider les gens à (re)trouver la joie et la fierté dans ce qu’ils accomplissent._

_**InfoQ FR : Pourriez-vous revenir sur votre parcours et ce qui vous a mené à votre pratique actuelle ?**_

> _**Dragos Dreptate** : Je me rappelle comme si c’était hier de ce jour quand, à 12 ans, j’ai touché pour la première fois le clavier d’un Z80 pour écrire mes premières lignes de code en Basic. J’étais fasciné, et à cet instant précis j’ai su ce que je voulais faire pour le reste de ma vie : créer des logiciels. Mon parcours fut tracé par cette passion pour les ordinateurs : après un diplôme d'ingénieur en génie logiciel obtenu à Cluj en Roumanie, je rejoins un éditeur de logiciels en France en tant que développeur. Mon rêve de faire de ma passion un métier se réalise ! Les années passent et, attrapé dans une carrière qui me porte vers des rôles de project manager et puis CTO, le rêve commence petit à petit à se transformer en désillusion. Je découvre une autre facette du métier de développeur, où la passion et l’envie de faire des choses extraordinaires sont troquées pour des postes qui payent mieux, où la compétence, l’initiative et la motivation sont matraquées à coup de bureaucratie, de jeux politiques et d'ego sur-dimensionné. On tue la vocation et la motivation intrinsèque._
>
> _Quand je découvre l’agilité en 2005, mon espoir renaît et je recommence à croire qu’on peut faire mieux. Après quelques années de pratique, je me tourne naturellement vers l'accompagnement, pour aider les organisations et les individus à donner du sens, trouver de la joie, être fiers de pratiquer leur métier. C’est pour ces mêmes raisons que je viens de rejoindre récemment beNext._

_**InfoQ FR : Concrètement, comment se traduit votre métier au quotidien ? Qu'est-ce que vous faites ?**_

> _**Dragos Dreptate** : La mission que je me donne, chaque jour, est d’aider des gens ordinaires à faire des choses extra-ordinaires. Souvent, cela nécessite d’apprendre à accepter l'incertitude et la complexité comme des faits, et de trouver des moyens plus adaptés pour y faire face. Pour réussir ce changement, il faut bousculer le statu quo, repenser les organisations et leur management, trouver des nouveaux modèles de fonctionnement au niveau des équipes et pour créer leurs produits. De vrais défis auxquels je cherche un début de réponse dans l'agilité, la pensée Lean, le Lean Startup ou encore le Design Thinking._
>
> _Au quotidien, cela prend des formes très variées car les solutions sur étagère sont une chimère. Adepte d'un coaching systémique, j’observe l’environnement et j’adapte mes interventions au contexte en mettant l’accent sur la collaboration, l’auto-organisation et l’amélioration continue, en créant les conditions pour faire émerger le changement, l’engagement et la motivation._
>
> _Coté produit, je travaille avec les équipes autour de la vision et la stratégie, les boucles de feedback, la pensée créative - un sujet que j’aurai l'occasion de partager lors d’une session à Agile France en juin._

**InfoQ FR : Quelles sont vos sources d’inspiration et de formation ?**

> _**Dragos Dreptate** : Ma principale source d’inspiration est la lecture, même si je n’arrive pas a y consacrer tout le temps que je voudrais. Ma liste de livres en attente est toujours trop pleine, l’encours aussi. Pour donner un exemple, en ce moment, je lis trois bouquins en parallèle : “Adapt” de Tim Harford qui explore une approche pour réussir dans un monde complexe ; “Joy Inc.” de Richard Sheridan sur comment créer une entreprise où la joie fait son succès - à lire absolument ; et “Thinking, Fast and Slow” de Daniel Kahneman sur le fonctionnement de notre cerveau et la prise de décisions. Ensuite, mes lectures alimentent les conversations avec des copains._
>
> _La communauté Agile est aussi un endroit inépuisable d’idées et d'échanges. Il y a une pléthore de meetups et de très bonnes conférences. Tout le monde peut y trouver quelque chose à apprendre. J’essaie d'y apporter ma modeste contribution en participant à l’organisation de Lean Kanban France et de ALE 2016 (Agile Lean Europe), une conférence itinérante en Europe qui aura lieu cette année à Paris._

**InfoQ FR : Pour vous, quelle est la partie la plus intéressante de ce que vous faites ?**

> _**Dragos Dreptate** : Selon mon expérience, le succès et la performance (sans entrer ici dans un débat sur la définition de ces mots) reposent principalement sur l’engagement et la motivation, qui a leur tour se construisent autour du sens de ce qu’on fait, de l’autonomie qu’on a pour le faire et de la maîtrise, la capacité à le faire. Le rôle du coach sur ces aspects me parait essentiel et pour illustrer ma vision, je me tourne à nouveau vers une citation d’Antoine de St-Exupery :_
>
>> _“Si tu veux construire un bateau, ne rassemble pas tes hommes et femmes pour leur donner des ordres, pour expliquer chaque détail, pour leur dire où trouver chaque chose... Si tu veux construire un bateau, fais naître dans le coeur de tes hommes et femmes le désir de la mer”._
>
> Adresser tous ces aspects humains au quotidien est certainement la partie la plus gratifiante de mon travail.

**InfoQ FR : A l'inverse, qu'est-ce qui vous paraît compliqué dans la posture du coach ?**

> _**Dragos Dreptate** : La “posture basse”, basée sur l’écoute, la reformulation, la clarification, est essentielle dans le coaching. Elle impose une forme de neutralité sur les sujets du client. C’est une vraie difficulté pour quelqu’un qui vient du monde technique et du produit. Intégrer mon expertise, mes expériences et mes convictions sans tomber dans le conseil ou l’influence est toujours un challenge pour moi. Mais ça reste la clé d’un accompagnement réussi et efficace._

**InfoQ FR : Quelles sont vos plus belles réussites en tant que coach ?**

> _**Dragos Dreptate** : Le changement est un processus lent, dans la durée. Pour se rendre compte du chemin parcouru, il faut prendre du recul et comparer l’avant et l'après sur une période plus longue : un groupe qui devient une équipe, un espace de travail terne et impersonnel qui se transforme pour faire place au visuel et à la collaboration. Et parfois de bonnes surprises avec des basculements rapides, des postures qui changent et des dynamiques de groupe qui se forment suite à deux jours passés ensemble pour une immersion dans l’Agile._

**InfoQ FR : Quels sont vos plus criants échecs ? Qu'en avez-vous appris ?**

> _**Dragos Dreptate** : Dépenser de l'énergie pour un impact très faible. Cela m’arrive parfois et c’est très dur à encaisser car inévitablement on peut douter de soi. C’est un moment de remise en cause, je cherche à comprendre ce que j’aurais pu faire de différent, un vrai moment d’apprentissage. C’est aussi un moment ou il faut relativiser car la cause de ces échecs est toujours un concours de facteurs complexes qui souvent, se trouvent en dehors de notre cercle d’influence, qu’on maîtrise peu ou pas, donc sur lesquels on ne peut pas directement agir._
>
> _Ce que j’ai appris avec le temps, c’est l’importance de bien définir le cadre de mes interventions, mon rôle, les attentes, les redevabilités de chaque partie. Un deuxième apprentissage est d’être à l'écoute, tout au long de la mission, des signaux faibles qui peuvent m’alerter que je fais fausse route._

**InfoQ FR : Comment percevez-vous le déploiement de l'Agile dans les organisations françaises ?**

> _**Dragos Dreptate** : Disons que c'est un peu surévalué. Pour paraphraser un dicton désormais célèbre, l’Agile me fait penser parfois au sexe à l’adolescence : tout le monde en parle, personne ne sait vraiment comment le faire, tout le monde pense que les autres en font, alors tout le monde prétend en faire. Mais surprenant aussi, car je rencontre parfois des structures qui ne sont pas tombées dans la mode du “faire de l’Agile” et qui pourtant ont plus d’agilité dans leur ADN que les autres. Ce qui compte avant tout, c’est la mentalité, l’état d’esprit. C’est ce qui fait au final la différence et permet de passer le gouffre entre faire de l’agile et être agile._

**InfoQ FR : Comment analysez-vous le développement du coaching agile sur les dernières années ?**

> _**Dragos Dreptate** : L'intérêt grandissant des organisations pour l’Agile a nourri le besoin en accompagnement, ce qui a conduit à une demande de plus en plus importante pour le coaching. Et naturellement un accroissement du nombre de coachs pour répondre à cette demande. C’est un bon signe, une dynamique vertueuse, mais la façon dont la plupart des organisations envisagent aujourd’hui leur accompagnement n’est pas efficace, car grandir ce n’est pas massifier. Pensez au coaching Agile comme à l'oxygène dans l'atmosphère : très utile, voire indispensable, mais quand il y en a trop, cela devient nocif. Il faut trouver le bon dosage entre une présence du coach trop faible pour déclencher et soutenir le changement, et une présence importante avec le risque que le coach devienne trop opérationnel et remplace les véritables acteurs._

**InfoQ FR : Si nos lecteurs veulent se lancer, par où leur conseilleriez-vous de commencer ?**

> _**Dragos Dreptate** : Premiers pas : répondre devant le miroir à la question “pourquoi voudrais-je être coach agile ?”. Ensuite, jeter un dé pour choisir un point de départ parmi ces citations et commencer à explorer :_
>
>> _"Aucun problème ne peut être résolu sans changer l'état d'esprit qui l'a engendré" - Albert Einstein_

>> _“On ne voit bien qu'avec le cœur, l'essentiel est invisible pour les yeux” - Antoine de Saint-Exupéry_

>> _“La perfection est atteinte, non pas lorsqu'il n'y a plus rien à ajouter, mais lorsqu'il n'y a plus rien à retirer” - encore Saint Exupery (oui, je suis un grand fan)_

>> _"Ce qu’on enseigne à l’enfant, on l’empêche de le découvrir" - Jean Piaget_

>> _"Si vous mettez des clôtures, vous allez avoir des moutons” - je cherche encore l’auteur..._

>> _“On ne peut pas se gratter avec la main d’un autre” - sagesse roumaine_

> Et une dernière pour être sûr qu’on s’est bien compris :)

>> _"Entre ce que je pense, ce que je veux dire, ce que je crois dire, ce que je dis, ce que vous voulez entendre, ce que vous entendez, ce que vous croyez en comprendre, ce que vous voulez comprendre, et ce que vous comprenez, il y a au moins neuf possibilités de ne pas se comprendre." - Bernard Werber_

**InfoQ FR : Quelles sont les tendances qui vous ont étonné récemment et que vous pourriez partager avec nos lecteurs ?**

> _**Dragos Dreptate** : L’approche Solution Focus, bâtie autour du constat que parler de problèmes crée des problèmes, parler de solutions crée des solutions. C’est un vrai changement de paradigme qui propose de construire des solutions au lieu de régler les problèmes. Et ça change tout._

_Je me réjouis aussi de l’ampleur prise par les idées portées par le mouvement de l’entreprise libérée, l’holacratie ou encore le livre Reinventing Organizations. Elles ont comme racine commune ce ressenti que notre manière actuelle de gérer les organisations touche à ses limites. Je veux croire que c’est l'émergence d’un nouveau paradigme d’organisation, qui remet l’humain au centre des préoccupations pour bâtir autour du sens et de la raison d’être._

[1]: https://www.infoq.com/fr
[2]: https://www.infoq.com/fr/vc
[3]: https://www.infoq.com/fr/author/St%C3%A9phane-Wojewoda
[4]: https://www.infoq.com/fr/articles/la-voix-du-coach-dragos-dreptate
[5]: https://fr.wikipedia.org/wiki/Les_Habits_neufs_de_l%27empereur