---
title: "Ne réduisez pas l'agilité à l’adaptation"
date: 2017-07-28
draft: false
#featured_image : "/images/leonardo.jpg"
aliases: [/2017/07/28/ne-reduisez-lagilite-a-ladaptation/]


---
![pieter-bruegel-la-tour-de-babel-750x566.jpg][8]

## L'intrigue

L'autre jour, une belle soirée d'été, lors de mes pérégrinations quotidiennes sur Linkedin, mon attention fut attirée par le commentaire élogieux d'une de mes relations à propos de l'article [Ce n'est pas d'agilité dont votre organisation a besoin][1]. Je n'ai pas l'habitude de réagir sur Linkedin mais ce soir-là, la solitude, ou les 2 bières, on eut raison de ma réserve vis-à-vis des débats sur la toile. Et comme Linkedin n'aime pas le longs commentaires, voici mes pensées réunies sous forme d'un article (j'entends déjà quelque uns me dire "il était temps, Dragos !"). C'est un article coup de gueule, une réaction épidermique contre cette forme de simplisme, de réductionnisme, cette façon récurrente de réduire l'agilité à un processus de delivery, ayant comme unique vertu l'adaptation poussée à l'extrême, de voir l'agilité comme une façon de réagir sans réfléchir, à chaud, sans prendre du recul.

Voici donc ma réponse à Denis, à qui je remercie de m'avoir fait découvrir l'article.

![bruegel-sorcieres-bruges-8-720x340][2]

## L'élément perturbateur

Oui, c'est un article intéressant. A force de vouloir être agile à tout prix, pour des raisons souvent floues ou inconnues, bonnes ou mauvaises, sans réfléchir à l'intention ou à l'impact recherché, vouloir être agile par effet de mode, on finit, comme l'auteur de cet article, à faire un amalgame et avoir une image réductrice, tronquée, ambiguë de l'agilité.

## Les péripéties agiles

Oui, l'agilité s'attaque à la complexité de notre monde actuel: volatile, incertain, ambigu. Oui, l'agilité admet cette complexité intrinsèque et comprend que, par nature, une chose complexe ne peut pas être "managée". Car les relations causes / effet ne sont pas connues à priori. On ne peut pas être prédictibles face à la complexité. Car il n'y a pas de certitudes. Au lieu d'analyser la situation, chercher la bonne solution, bâtir un plan et le dérouler ensuite, une meilleure manière d'agir est de faire de petits pas, analyser les effets de nos actions, comprendre ce qui est en train de se passer, et s'adapter en fonction du feedback. Nos actions produisent-elles les effets escomptés? On continue, on fait plus de ça. Nos actions ne sont pas efficaces, voire contre-productives? On s'adapte, on cherche de nouveaux chemins, on fait différemment. Naviguer à vue, avancer en tâtonnant.

Donc oui, l'agilité est fondé sur le principe d'adaptation. Mais voir l'agilité uniquement comme de l'adaptation, du court-termisme, de la réaction à chaud serait réducteur. Comme si on considérait qu'un skipper, parce qu'il adapte en permanence ses mouvements, sa trajectoire, ses paramètres de navigation, serait un errant dans les océans, sans cap ni destination. On ne gagne pas le Vendée Globe en traversant l'océan en ligne droite. On le gagne en gardant un cap, suivre une destination, tout en adaptant sa route au gré des inconnus qu'on rencontre sur notre chemin. C'est en cela que je trouve la vision présentée par l'article comme étant tronqué, faussé par un regard réduit à une seule perspective sur une réalité complexe que l'auteur tente de simplifier.

## L'aventure

Réussir à innover, à faire la différence, demande une vision bien plus holistique. Être agile est aussi et surtout donner du sens. Commencer avec le pourquoi. Chercher à comprendre le problème avant de se jeter sur la solution. Réfléchir à l'impact et aux bénéfices qu'on cherche à atteindre. Se concentrer d'abord sur les l'impact (les résultats) avant la production (les livrables). Savoir comment nous allons mesurer cet impact. Car pour s'adapter, nous avons besoin de savoir si nos actions produisent les résultats souhaités. Donc nous avons besoin de feedback. Du feedback mesurable, quantifiable si vous voulez.

C'est l'essence du mouvement [Lean Startup][3] (popularisé par le livre d'Eric Ries), qui structure la création d'un produit ou service autour d'une boucle "Build / Measure / Learn". J'ai une idée, je fais une hypothèse. Je crée un experiment (en minimisant son coût et sa durée) me permettant de valider ou invalider mon hypothèse, d'apprendre. Je mets le résultat de cet experiment dans les mains des "utilisateurs" et je mesure si ça marche ou pas, j'apprends. J'en tire les conséquences, soit en continuant sur cette piste soit en pivotant car l'idée initialement brillante s'avère sans avenir. Car comme disait Von Molkte:

> Aucun plan ne survit au premier contact avec l'ennemi

Cela passe aussi par la gestion des échelles de temps multiples. Une idée bien présenté par Geoffrey Moore dans son livre [Escape Velocity][4] où il décompose la stratégie en trois horizons : « horizon1 », celui du court-terme (de 0 à 12 mois), de l'atteinte des résultats opérationnels, du « delivery », « horizon 2 », de 12 à 36 mois, celui de la croissance, de la construction de ce qui permet cette croissance ou de l'élimination de ce qui la bloque, et « horizon 3 », au delà de 36 mois, le temps de l'exploration des options, pour déterminer ce qui sera construit en horizon 2 et exploité en horizon 1. Savoir doser l'effort, les ressources, entre ces trois horizons, réussir à délivrer et générer du cash flow sans négliger l'investissement dans la recherche du prochain coup gagnant, et tout ça gardant ou adaptant le cap de l'entreprise à long terme, sentir ou elle veut aller et lui permettre d'y parvenir avec une stratégie adaptée, est un exercice d'équilibrisme et jongle dont peu de leaders en font preuve.

## Dénouement

Si ses quelques lignes captent votre attention et vous aimeraiez creuser un peu plus le sujet, je vous recommande un dernier livre, [Lean Entreprise][5] de Jez Humble, Barry O′Reilly et Joanne Molesky, qui donne une vision globale, holistique, sur ce que c'est une entreprise agile et met en avant l'importance de relier ensemble le product discovery (que devons nous faire) et le product delivery (comment nous le faisons). Je vais revenir sur ce livre dans un prochain article à l'occasion du passage en France d'une de ses auteurs, Joanne Molesky, qui sera présente fin Novembre à [Lean Kanban France][6] lors d'une session sur "Lean Enterprise Or Growing an Innovative Sulture". Venez nombreux 😉



[1]: https://philippesilberzahn.com/2017/07/24/pas-agilite-dont-votre-organisation-a-besoin/amp/
[2]: /images/bruegel-sorcieres-bruges-8-720x340.jpg
[3]: https://www.amazon.fr/Lean-start-up-Eric-Ries/dp/2744065080
[4]: https://www.amazon.com/Escape-Velocity-Geoffrey-Moore-ebook/dp/B005MRBZQO
[5]: https://www.amazon.fr/Lean-Enterprise-Jez-Humble/dp/1449368425
[6]: http://www.leankanban.fr/
[8]: /images/pieter-bruegel-la-tour-de-babel-750x566.jpg