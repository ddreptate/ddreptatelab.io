---
title: "Séance de fitness pour la pensée créative"
date: 2016-05-29
draft: false
aliases: [/2017/05/29/seance-de-fitness-pensee-creative/]


---
![le velo de carelman](/images/jasques-carelman-objet-introuvable-08.png)

## Le clichée

Vous l'avez sûrement déjà entendu, subi ou même décrété:

> _Think out of the box!_
> 
> _(NDTR: sortez du cadre, pensez sans idées préconçues, sortez des sentiers battus)_

C'est un clichée, un non-sens qui revient régulièrement dans les séances de brainstorming, lors de la recherche de nouvelles idées ou dans toute autre activité où "on se doit" d'être créatifs. Lors de mes interventions auprès des équipes produit, je l'entends souvent, comme une incantation de la part de managers. Comme si la simple énonciation de ce souhait transformerait d'un coup tous les participants en êtres plus créatifs.

## La cause

Pour moi, la principale cause est une mauvaise compréhension de la nature même de la créativité. La réalité n'est pas aussi simple. Décréter la créativité c'est facile. Devenir créatif, c'est une tout autre histoire. Je parle de "devenir" car la créativité, contrairement a certains idées reçues, n'est pas un don, un talent qu'on a ou on n'a pas. On ne naît pas créatifs ou pas. Ou plutôt on est tous créatifs à la naissance (des nombreuses études sur les enfants le démontre) mais avec le temps on perd cette faculté. Pourquoi? Car dans le processus d'apprentissage, de découverte du monde, de développement des nos capacités cognitives, nous construisons des modèles mentaux qui nous empêchent ensuite de voir le monde tel qu'il l'est. On le voit tel que nous sommes. On le voit à travers de ce que nous sommes, à travers des règles dans notre subconscient, de nos suppositions, de contraintes qu'on s'impose, de nos peurs.

> _"Discovery is seeing what everyone else has seen and thinking what no one else has thought"_
> 
> _– Albert Szent-Gyorgyi_

## Que faire?

Alors, comment devenir (ou redevenir) créatifs? Comment sortir de ce cadre, de cette boite qu'on nous reproche tant? Cela demande tout d'abord de la pratique, de faire travailler ce véritable "muscle" de la créativité. D'apprendre à aller au delà de notre expérience, de nos croyances, de ce qui nous est connu. De sortir de cette zone de confort, rassurante, pour aller vers l'invisible, l'inconnu. La créativité commence la où on ne se sent plus à l'aise, où on ressent une sensation de risque, un blocage, une envie soudaine de revenir en arrière, vers le connu, vers le normal. Une envie de revenir dans le cadre. Là, on est pleinement dans la créativité!

> _"Genius is one percent inspiration, ninety-nine percent perspiration."_
> 
> _– Edison_

Avec [Alexandre Quach][1], nous avons imaginé une véritable [séance de fitness pour la pensée créative][2]. Un cocktail d'exercices pour découvrir des techniques conçues pour stimuler, entraîner et garder en forme votre muscle de la créativité à découvrir lors de [Agile France][3] le 16 et 17 juin au Chalet de la Porte Jaune.

 
<iframe src="//www.slideshare.net/slideshow/embed_code/key/8grqiXnk8QfqIR" width="100%" height="550" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> 
 

[1]: https://fr.linkedin.com/in/alexandrequach
[2]: http://lanyrd.com/2016/agilefrance/sfbfkq/
[3]: http://lanyrd.com/2016/agilefrance/

  