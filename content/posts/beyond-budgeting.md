---
title: "Beyond Budgeting - vers une entreprise agile"
date: 2021-02-22
draft: false
image: /images/Beyond_Budgeting_Principles.png
---
Le 12 février j’ai eu le plaisir de faire une présentation à [Frug’Agile][1] (un grand merci aux organisateurs pour l’invitation) sur le thème du Beyond Budgeting. Dans cet article je retranscris les notes qui m’ont servi à préparer cette intervention. L’enregistrement vidéo suivra bientôt avec, en complément, une session d’une trentaine de minutes de Q&A. 

Je suis parti initialement sur une présentation “classique” du Beyond Budgeting. Mais une fois la session retenue, les organisateurs nous ont apporté plus de précision en précisant la clé de lecture, le fil rouge, la ligne conductrice qu’ils souhaitaient donner à nos interventions. 

La question qu’ils nous ont posée fut : “comment tirer le bilan de ces 20 dernières années d’agilité et bien débuter les 20 prochaines?”

Je vais vous partager mes réflexions autour de cette question, au travers d’une des lectures possibles, une perspective parmi plein d’autres que nous pouvons avoir sur ces 20 ans d’agilité et la suite à venir. En parlant de perspectives, j’ai beaucoup apprécié le regard critique proposé par [Pablo][2], regard que vous pouvez retrouver [ici][3].

Donc, revenons à notre question: “comment tirer le bilan de ces 20 dernières années d’agilité et bien débuter les 20 prochaines?”

## AGILE - NAISSANCE, ENFANCE, ADOLESCENCE

Mettons un peu les choses en perspective et donnons un peu de contexte. 

La plupart des organisations naissent petites et agiles. Au début, elles sont flexibles, collaboratives, non bureaucratiques et orientées vers un but précis. Mais rares sont celles qui veulent rester petites dans la durée. La plupart souhaitent se développer et devenir plus grandes. Et dans ce but, elles mettent en place des nouveaux processus, de nouvelles structures, de nouvelles formes d’organisation et de management, toutes censées les aider à se développer, à grandir. 

Celles qui y parviennent ont souvent quelque chose en commun. Elles découvrent qu'elles ne sont pas seulement devenues grandes. Elles sont également devenues lentes, rigides, bureaucratiques, détournées de leur but et déconnectées de leurs clients. L'engagement et la motivation se sont effondrés, le turnover a augmenté, les clients râlent et les performances s'érodent. Les moyens qui les ont aidés à devenir plus grandes sont, petit à petit, devenus des freins.

En faisant ce constat, certains dirigeants se posent, naturellement, la question:  "Comment retrouver ce que nous avions quand nous étions petits, sans perdre les avantages d'être grands ?”

De même, les petites organisations qui veulent se développer et qui regardent ceux qui ont déjà fait ce chemin, se posent aussi la question: "Comment pouvons-nous grandir sans nous retrouver dans la même situation ?".

Quand l’agilité est apparue, il ne s'agissait pas de challenger le fonctionnement des grandes organisations ni de les aider à se réinventer. Bien que le mouvement Agile se soit rebellé contre de nombreux aspects du management traditionnel, l'accent a été mis au début sur l'impact négatif que cette approche managériale avait sur le développement logiciels et sur la manière dont les équipes pouvaient mieux travailler. L’ambition initiale n’était pas de transformer la façon dont l'organisation entière était gérée pour la rendre plus agile mais de “mieux développer des logiciels” en proposant un cadre de valeurs et de principes pour y parvenir.  

## AGILE - L'ÉMANCIPATION

Mais ces valeurs et principes proposés il y a 20 ans restent pertinentes, valides, applicables bien au-delà du monde du développement logiciel. 

C’est pourquoi, petit à petit, l’agilité s’est créé un chemin en traversant de plus en plus la frontière de l’IT pour remettre en question la façon dont des entreprises pensent la planification, la synchronisation à l’échelle, la gestion des flux de valeur, casser les silos, et de nombreuses autres croyances en matière de management et organisation. Une communauté Agile dynamique, grandissante, passionnée - et passionnante - se forme et avec elle c’est l’émergence d’une nouvelle mentalité, de nouvelles méthodes de travail, de nouvelles pratiques.

Et nous voici, 20 ans plus tard. Il faut se le dire, malgré un certain agile-bashing, les résultats sont importants. L’agilité a changé profondément la façon dont nous développons des logiciels et est devenue la nouvelle norme. Il n'y a pas de controverse à cet égard. Personne ne parle de revenir en arrière. Il s'agit aujourd’hui plutôt de le faire fonctionner encore mieux, de percer au-delà de l’IT, de l'étendre à l’ensemble de l’entreprise. 

## AGILE - L'AGE DE LA MATURITÉ

Mais tout n'est pas rose. Il y a encore des frictions. Le fait que l’IT et, plus tard, d'autres fonctions aient adopté l’agilité dans leur fonctionnement interne, n'a pas résolu les nombreux conflits entre les processus traditionnels et la philosophie agile. 

Des nombreuses initiatives autour de l’agilité à l’échelle sont apparues comme une réponse naturelle, basée sur la conviction que toute l'organisation doit fonctionner selon cette nouvelle philosophie. Il s'agit d'une suite naturelle, une étape qui vient de démarrer depuis quelques années, mais qui n’est encore qu'à ses débuts. Elle s'accompagne aussi de quelques mises en garde.

Premièrement, il est difficile d'étendre l’agilité à l’ensemble de l’entreprise en utilisant le même cadre et le même langage qui ont si bien fonctionné pour transformer le développement de logiciels. "Scrum" pourrait faire penser à une maladie de peau pour ceux peu familiers avec le rugby. Un "Sprint" n’a rien à voir avec une course de vitesse. Et une "livraison continue" n'est pas une question d'efficacité des chaînes de montage.

Nous avons besoin d'une traduction, quelque chose qui aide les équipes de direction à mieux comprendre ce que signifie l'agilité à l’échelle de l'entreprise. Une traduction permettant un dialogue sur la notion d’entreprise agile en utilisant un langage qui leur est propre, qu’ils aient envie d'écouter et qu’ils seraient capables de comprendre.

La deuxième mise en garde porte sur le fait qu’il reste un éléphant dans la pièce, un problème dont l’agilité souffre depuis ses débuts. Bien qu'il y ait eu des tentatives pour l’adresser, peu de choses se sont passées. Peut-être parce qu'il était considéré comme quelque chose immuable, intouchable.

Vous l’avez compris, je fais référence au budget, probablement l'obstacle le plus coriace sur le chemin vers une entreprise plus agile. Pas seulement le budget annuel et le processus budgétaire lui-même, mais tout autant la mentalité qui est à son origine. 

> “Le budget est le fléau des entreprises [...], le processus le plus destructeur de valeur”
>
> -- Jack Welch
>


## UN MODÈLE OBSOLÈTE

Un peu d’histoire. La budgétisation est une méthode de gestion ancienne. Il y a presque 100 ans, son inventeur, James McKinsey, la popularisa au travers de son livre "Budgetary control". On est fin du XIX début du XX siècle , en pleine révolution industrielle. C’est aussi l’époque où Ford et Taylor mettent les bases de leur théorie de management. Ils proposent un modèle d’une organisation vue comme un engrenage mécanique, avec un travail normé, qui cherche à maximiser le rendement.

Toutes ces intentions étaient bonnes : leur but était d’aider les organisations à être plus performantes. Et cela a très bien fonctionné à l'époque, car le contexte était complètement différent de celui d'aujourd'hui.

Mais par leur nature, le processus budgétaire et la théorie de management de Taylor sont, à bien des égards, l'antithèse de l'agilité. 

Elles reposent sur deux postulats. 

Premier postulat : le monde est stable, les choses ne changent que peu et lentement et l'avenir est prévisible. Basé sur cette croyance, le mode opératoire proposé part d’une analyse détaillée et rigoureuse des techniques de production, des gestes, des rythmes, des cadences, pour établir la « meilleure façon » de produire par une délimitation et un séquençage des tâches. C’est l’entreprise silo, c’est ce qu’on appelle communément du “command & control”. 
 
Deuxième postulat : la croyance qu’on ne peut pas faire confiance aux gens, que l’homme n’est pas motivé par le travail.

Dans son livre “The human side of the Entreprise”, McGregor développe sa théorie X et théorie Y, et identifie deux archétypes de pensée. 

Le premier, qu’il l’appelle X est celui qui pense que l’être humain est paresseux et n’aime pas travailler. S’il n’aime pas travailler, on peut pas le laisser en autonomie, il faut mettre en place une forme de contrôle. Et comme l’homme n’est pas motivé, il faudra aussi trouver des mécanismes pour le forcer à produire l’effort attendu. Ces mécanismes prennent la forme de récompenses, de contraintes, ou bien de punitions. Pour l’archétype X, l’employée ne prendra jamais des responsabilités et des initiatives donc il faudra bien lui dire quoi faire.Et cerise sur le gateau, il va utiliser toute sa créativité et son intelligence pour contourner les règles en place et se soustraire au travail qu’on attend de lui.

L'archétype Y c’est tout le contraire, il pense que l’homme est intrinsèquement motivé et , s’il est mis dans des bonnes conditions, il s'avère capable de travailler en autonomie, de s’améliorer et de prendre des responsabilités. 


## UN MONDE PLUS COMPLEXE
 
Le monde dans lequel McKinsey, Ford et Taylor ont mis les bases de théories, ce monde là est bien dépassé. Aujourd’hui tout s'accélère, la concurrence est plus intense, la technologie et le travail de la connaissance prennent une place de plus en plus importante. Tout cela accroît l'incertitude et la complexité à laquelle les entreprises doivent faire face. On parle d’un monde VUCA, un acronyme que vous avez très probablement déjà entendu (volatile, incertain, complexe et ambigu)

Avec cette nouvelle donne, on va plutôt chercher une capacité à réagir et s’adapter plutôt que planifier et exécuter. On comprendra qu'on ne peut pas “manager” la performance ou l’innovation. On ne peut pas agir là-dessus directement. On va donc plutôt chercher à créer un contexte favorable pour que la performance et l’innovation aient lieu. Ça nous rappelle la Théorie Y de McGregor. 

La valeur est devenue souvent immatérielle, intangible, on manipule de la connaissance. Sa création, je parle de la valeur, va venir de l’implication des personnes et de la capacité du système à apprendre et à s’adapter rapidement.


## LES PROBLÈMES DU BUDGET

Et dans ce nouveau contexte, le modèle précédent, qui s’est construit autour d’un monde stable et de la capacité à faire des prévision, s'écoule. Le processus budgétaire, qui est structurant pour le fonctionnement de l’entreprise, est devenu un obstacle à la performance et à l’innovation dans nos organisations. 

La réalité est peu flatteuse. Il est décorrélé de la stratégie de l’entreprise et plutôt calqué sur l’architecture de l’organisation. Il demande un travail fastidieux, pour prévoir dans les moindre détails l'année suivante avec une précision comptable. 

Par sa construction, la fenêtre de tir est réduite. C’est comme si la banque était ouverte seulement deux mois par an et qu’on doit prévoir à ce moment-là, la moindre dépense sur l’année à venir sinon ce sera trop tard. Que se passera-t-il si au cours d’année une bonne idée, qui n’a pas été prévue dans le budget, se présente ? 

L’aveuglement est à son paroxysme : tout écart au budget initial est traité comme un élément négatif qui doit être expliqué ; toute action et décision fait l'objet d'une micro-gestion bureaucratique. 

Il s'agit d'un exercice biaisé et politique qui stimule les comportements non éthiques et nuit à la prise de décision. L’évaluation des performances est réduite à l’atteinte des chiffres du budget. L'objectif à long terme, la stratégie, les valeurs deviennent une plaisanterie parce que tous les yeux sont tournés vers ces chiffres. 

Et tout cela prend énormément de temps.


## BEYOND BUDGETING

Le mouvement _Beyond Budgeting_ est apparu à la fin des années ‘90 en réaction à tous ces effets négatifs. 

Le mot _budgeting_ dans _Beyond Budgeting_ est un peu trompeur, car en réalité il s’agit d’une philosophie alternative au modèle traditionnel de management (avec le processus budgétaire annuel au centre). Le focus est mis sur l’optimisation de la création de valeur plutôt que de contrôler les coûts. 

_Beyond Budgeting_ décrit à la fois une culture de leadership et un système de management de la performance. Au travers de ses principes et pratiques, il incarne une vision possible de l’entreprise agile.

Entreprise Agile? De quoi parle-t-on exactement? Je vous propose ma définition: une entreprise agile est celle qui présente la capacité de délivrer de la valeur dans un contexte mouvant et incertain, de donner du sens à sa mission et de construire avec et pour ses collaborateurs et ses clients. 

![beyond budgeting principles](/images/Beyond_Budgeting_Principles.png)

Quand on regarde tous ces éléments, on peut se dire que ce n'est probablement pas une coïncidence si _Beyond Budgeting_ et Agile sont nés à peu près en même temps. Et pas une coïncidence non plus si les 12 principes de _Beyond Budgeting_ ont tant en commun avec le Manifeste Agile, même s'il n'y a pas eu de contact entre les deux communautés à l'époque.

S'il existe de nombreuses similitudes avec le Manifeste Agile, il y a aussi une différence majeure. Comme évoqué auparavant, en raison de son focus sur le développement de logiciels et de l'accent mis sur le fonctionnement des équipes, certaines questions sur la gestion d'entreprise étaient tout simplement hors scope au début du mouvement Agile. Comme par exemple le budget annuel. 

_Beyond Budgeting_, au contraire, est né comme une alternative à la gestion traditionnelle de l'entreprise, et traite donc des nombreuses questions structurantes que le mouvement Agile ne s’est pas posé au tout début. Ainsi, on peut voir le _Beyond Budgeting_ comme le maillon manquant d'une transformation à l’échelle de l’entreprise pour devenir plus agile. Car il fournit non seulement un vocabulaire sur l'agilité à l’échelle de l’entreprise que les dirigeants comprennent, le modèle offre également des alternatives concrètes au processus budgétaire traditionnel, ainsi qu'une cohérence avec d'autres processus de management tels que l'évaluation des performances, les primes et les bonus.

Ces principes de leadership, comme vous pouvez le voir, ne sont toutefois pas du tout uniques à _Beyond Budgeting_. De nombreux autres mouvements et modèles, dont l’agilité, sont construits sur des convictions très similaires. 

J’ouvre une petite parenthèse, car c’est un vaste sujet, quand on parle de Lean Startup, Design Thinking, Holacratie, Management 3.0, Cynefin, etc., les fondations, les principes, la philosophie et les intentions sont toujours très proches voire similaires. Tous ces mouvements ont comme but de faire évoluer la façon dont les entreprises fonctionnent, s’organisent, travaillent. C’est ce que nous entendons en général quand on parle de “entreprise agile”.

Revenons. Et si on regarde les six principes concernant les processus de management, on observe qu'il s'agit avant tout d'activer des leviers importants pour les principes de leadership, comme le but, l'autonomie, les valeurs et la transparence. Il y a une relation étroite, dans les deux sens, entre les processus et la culture, elles s'influencent mutuellement à la fois positivement et négativement. Car il n’est pas très utile d'avoir un leadership moderne, visionnaire si les processus restent calqués sur un modèle obsolète. Il en résulte des dissonances entre ce que nous prêchons et ce que nous pratiquons, chose malheureusement constatée dans trop d'organisations. 

Un message clé de _Beyond Budgeting_ concerne donc la nécessité d'une cohérence entre les principes de leadership et les processus de management. Un exemple de tension entre ces principes et les processus de management est incarné par l’opposition fréquente entre la direction Financière et celle des Ressources Humaines qui sont responsables, chacune, d’un des deux processus les plus détestés de l'entreprise : le budget et l'évaluation des performances. Tout comme la processus budgétaire traditionnel est l'antithèse de l'agilité, on peut en dire autant de nombreux processus d'évaluation et des systèmes de primes et bonus qui y sont liés.

Ils affectent tous deux l'ensemble de l'organisation, directement ou indirectement. Apporter des changements radicaux dans ce domaine peut envoyer un message extrêmement fort à tout le monde, pour signaler que la transformation est bien réelle. Tout comme le fait de ne pas y toucher transmet le message inverse, à savoir qu'il n'y a pas de changement réel, en profondeur. Quand les finances et les ressources humaines unissent également leurs forces pour soutenir la transformation, cela envoie un message encore plus fort sur la volonté d’une entreprise de changer.

Regardons maintenant de plus prêt les quatre premiers principes liés au budget:  le rythme, la définition des objectifs, la planification, les prévisions et l'allocation des ressources. 

Commençons par se poser une question légitime : pourquoi établissons-nous des budgets ? A cette question, trois raisons différentes reviennent généralement. 

Premier: le budget a pour but d'allouer des ressources, de distribuer de l'argent pour les dépenses et les investissements. Deuxième: il sert également à fixer des objectifs. Des objectifs financiers, de vente ou de production. Enfin, le budget permet de prévoir et de comprendre ce qui nous attend, par exemple en termes de trésorerie et de capacité financière. Traiter ces trois objectifs en un seul processus et une seule série de chiffres peut sembler très efficace. Mais c'est là que réside aussi le problème. 

Imaginez le début d'un processus budgétaire, quand la Finance veut comprendre les flux de trésorerie de l'année à venir. En commençant par les recettes, on demande aux managers leur meilleur chiffre. Mais tout le monde sait que ce qui est envoyé en haut a tendance à revenir comme objectif, et souvent il sera lié aussi à un bonus. Nous savons tous ce que cette situation peut entraîner comme conséquence sur les chiffres soumis.

Par la suite ils vont questionner les dépenses et les investissements. Là aussi, tout le monde sait que c'est la seule chance d'obtenir des ressources pour l'année prochaine, et beaucoup se souviendront peut-être aussi de la réduction du chiffre soumis l'année dernière. Une fois encore, il ne faut pas s'étonner si cette situation influence également ce qui est proposé.

Nous connaissons tous ce jeu. On peut même être tentés de dire "c'est comme ça". Mais c'est en fait un problème sérieux. Non seulement parce qu'il détruit la qualité des chiffres, mais plus encore parce que ce jeu stimule des comportements nuisibles, à la limite de l'éthique.


## ALLER AU-DELÀ DU BUDGET

Alors, que faire? _Beyond Budgeting_ propose une solution simple et efficace : la séparation de ces trois usages en trois processus différents, car ils ont des finalités différentes.

Un objectif, une cible est une aspiration, ce que nous voulons qu'il arrive. Une prévision est une projection, ce que nous pensons qu'il va se passer, peu importe si nous aimions ou non ce que nous voyons. Regarder les chiffres sans se mentir. Et l'allocation des ressources est une question de choix d’utilisation et d'optimisation des ressources afin de maximiser les chances d’atteindre les objectifs. En les séparant, nous nous autorisons à disposer des chiffres différents en fonction de leur finalité. Un cible doit par exemple être plus ambitieuse qu'une prévision. 

Mais, plus important encore, cette séparation permet d'améliorer considérablement chacun de ces trois processus. Nous pouvons désormais rendre nos objectifs beaucoup plus inspirants et plus solides, nous pouvons éliminer les aspects politiques de nos prévisions et nous pouvons rendre notre allocation de ressources beaucoup plus efficace. Et enfin, nous pouvons adopter pour chacun de ces processus un rythme plus adapté à sa mission, en tenant compte par exemple de l'activité dans laquelle nous nous trouvons.

![au-delà du budget](/images/au-dela-du-budget.png)

Cette séparation peut devenir un catalyseur pour des discussions encore plus importantes.Pour la définition des objectifs on va pouvoir se poser la question de ce qui motive réellement les collaborateurs? Concernant les prévisions on pourra par exemple s'interroger sur ce qui cause vraiment les biais et la politique dans la construction de ces chiffres ? 

Cette séparation est une porte dérobée vers une discussion plus large sur le thème _Beyond Budgeting_ (en français "au-delà du budget") une étape essentielle sur le chemin vers une entreprise agile.

Car ne nous trompons pas, ce qui en surface peut paraître comme un sujet éminemment lié au processus budgétaire, très financier, très technique, est en fait un vrai cheval de Troie qu’on va utiliser pour se poser des questions importantes et initier des changements profonds à l’échelle de l’ensemble de l’entreprise. Et en ce sens, _Beyond Budgeting_ est juste une autre brique, une approche supplémentaire à notre panoplie de leviers pour aider les organisations à évoluer et devenir plus agiles. 
 

[Le support de présentation][4]

[La vidéo][5]

[1]: https://www.frugagile.org/
[2]: https://pablopernot.fr/
[3]: https://pablopernot.fr/2021/02/agilite-regard-critique-sur-les-dernieres-annees-pour-bien-debuter-les-suivantes/
[4]: https://fr.slideshare.net/ddreptate/beyond-budgeting-vers-une-entreprise-agile
[5]: https://youtu.be/SGeXPJoBQA4