---
title: "Changements"
date: 2019-03-29T22:51:07+01:00
draft: true
show_reading_time : true
---
Comment, dans les relations humaines, les impasses apparaissent-elles? Qu'est-ce qui fait que, souvent, nos tentatives de provoquer un changement ne font que nous emmurer dans un jeu sans fin? Il y a des changements qui ne sont que source de la permanence. Dire "plus ça change, plus c'est la même chose" équivaut, si l'on prend les choses par l'autre bout, à affirmer que ça change quand on s'y attend le moins. La technique ici proposée se situe delibérement à la *surface*: barrer le *pourquoi* pour mettre en avant le *quoi*. Un chemin est frayé dans la fôret des paradoxes humains, qui va de Russell, Wittgenstein et Bateson à Groucho Marx. Entre la logique et l'humour, une thérapie est mise en place qui ne prétend point guérir autre chose que notre rapport à autrui. 

Ce livre, écrit par trois personnalités marquantes du Mental Research Istitute, est l'un des ouvrages fondamentaux de l'Ecole de Palo Alto.

**Traduit de l'anglais (Etats-Unis) par Pierre Furlan**

Cela est de la grosse daube pour ne pas dire de gros mots et me faire punir et de réster dans le cadre *etudiant*           